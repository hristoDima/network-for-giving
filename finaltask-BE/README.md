# Final task 
created by H. Dimitrov

### Project Setup

```
<parent>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-parent</artifactId>
    <version>2.2.6.RELEASE</version>
    <relativePath/> <!-- lookup parent from repository -->
</parent>
```

Version 2.2.6 is mandatory, some things doesn't work in 2.3.0 !


##### Maven dependencies added
1. spring-boot-starter-web
2. h2
3. spring-boot-starter-jdbc
4. spring-boot-starter-test
4. spring-boot-starter-security
5. spring-security-test
6. lombok //logging
7. spring-boot-starter-log4j2


### REST API


#### Show user

Returns json data about the currently logged in user

- **URL** 

    /user

- **Method:**

    `GET`
    
- **URL Params

  
## Get All Tasks

Returns list of all tasks.
```
GET /tasks
```

### Parameters

| Name    | Type        | Description                                                    |
|---------|-------------|----------------------------------------------------------------|
| status  | TaskStatus  | Optional, if provided returns only tasks with the given status |



| Title    | Type        |
| URL  | TaskStatus    |
|sadnkjadns | asjndkjdas |



| <!-- -->        | <!-- -->       | 
|: -------------: |:-------------:|
| col 3 is      | right-aligned | 
| __Bold Key__      | centered      | 
| zebra stripes | are neat      |

 | | |
|-|-|-|
|__Bold Key__| Value1 |
| Normal Key | Value2 |

### Response

```
Status: 200 OK

[
    {
        "id": 1,
        "title": "Read the README file",
        "details": "Read the assignment.",
        "completed": true
    },
    {
        "id": 2,
        "title": "Implement get all tasks endpoint",
        "details": "Implement GET /tasks.",
        "completed": false
    }
]
```