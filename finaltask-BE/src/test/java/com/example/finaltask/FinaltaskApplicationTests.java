package com.example.finaltask;

import com.example.finaltask.models.User;
import com.example.finaltask.repositories.IUserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FinaltaskApplicationTests {

	@Autowired
	IUserRepository userRepository;


//	@Test
//	public void contextLoads() {
//
//	}

	@Test
	public void getUserByExistingUsernameTest(){
		final String USERNAME = "admin";
		User user = userRepository.getUserByUsername(USERNAME);
		assertEquals(USERNAME, user.getUsername());
	}

	@Test
	public void getUserByNonExistingUsernameTest(){
		final String USERNAME = "random";
		User user = userRepository.getUserByUsername("random");
		assertNull(user);
	}
}
