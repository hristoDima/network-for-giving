package com.example.finaltask.repositories;

import com.example.finaltask.models.User;
import com.example.finaltask.utillity.ExceptionLoggerUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;

@Slf4j
@Repository
public class UserRepository implements IUserRepository {
    private JdbcTemplate jdbcTemplate;
    private BasicUserRepository basicUserRepository;

    public UserRepository(JdbcTemplate jdbcTemplate, BasicUserRepository basicUserRepository) {
        this.jdbcTemplate = jdbcTemplate;
        this.basicUserRepository = basicUserRepository;
    }

//    @Override
//    public List<User> getAllUsers() {
//        String sql = "SELECT * FROM user";
//        try {
//            return this.jdbcTemplate.query(sql, this::mapUserRow);
//        } catch (DataAccessException e) {
//            ExceptionLoggerUtil.selectQueryLog(sql, e);
//            return null;
//        }
//    }

    @Override
    public User getUserById(int id) {
        String sql = "SELECT * FROM user WHERE user.id=? ";
        try {
            return jdbcTemplate.queryForObject(sql, this::mapUserRow, id);
        } catch (DataAccessException e) {
            ExceptionLoggerUtil.selectQueryLog(sql, e);
            return null;
        }
    }

    @Override
    public User getUserByUsername(String username) {
        String sql = "SELECT * FROM user WHERE user.username=?";
        try {
            return jdbcTemplate.queryForObject(sql, this::mapUserRow, username);
        } catch (DataAccessException e) {
            ExceptionLoggerUtil.selectQueryLog(sql, e);
            return null;
        }
    }


    @Override
    public void addUser(User user) {
        String sql = "INSERT INTO user (fname, lname, gender, age, location, username, password) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?)";

            jdbcTemplate.update(sql, user.getFname(), user.getLname(), user.getGender(), user.getAge(), user.getLocation(), user.getUsername(), user.getPassword());
    }

    @Override
    public void updateUser(int id, User user) {
        String sql = "UPDATE user " +
                "SET fname = ?, lname = ?, gender = ?, age = ?, location = ?, username = ?, password = ? " +
                "WHERE id = ?";
            jdbcTemplate.update(sql, user.getFname(), user.getLname(), user.getGender(), user.getAge(), user.getLocation(),
                    user.getUsername(), user.getPassword(), user.getId());
    }

//    @Override
//    public void deleteUser(int id) {
//        String sql = "DELETE FROM user WHERE id=?";
//        jdbcTemplate.update(sql, id);
//    }


    private User mapUserRow(ResultSet rs, int rowNum) throws SQLException {
        int id = rs.getInt("id");
        String firstName = rs.getString("fname");
        String lastName = rs.getString("lname");
        String gender = rs.getString("gender");
        int age = rs.getInt("age");
        String location = rs.getString("location");
        String username = rs.getString("username");
        String password = rs.getString("password");

        User user = new User(id, firstName, lastName, gender, age, location, username, password);
        return user;
    }
    

}
