package com.example.finaltask.repositories;

import com.example.finaltask.models.BasicUser;
import com.example.finaltask.models.Charity;
import com.example.finaltask.models.CharityVolunteer;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class CharityVolunteerRepository implements ICharityVolunteerRepository {
    private JdbcTemplate jdbcTemplate;
    private ICharityRepository charityRepository;
    private IBasicUserRepository basicUserRepository;

    public CharityVolunteerRepository(JdbcTemplate jdbcTemplate, ICharityRepository charityRepository, IBasicUserRepository basicUserRepository) {
        this.jdbcTemplate = jdbcTemplate;
        this.charityRepository = charityRepository;
        this.basicUserRepository = basicUserRepository;
    }

    @Override
    public List<Charity> getCharitiesForUser(BasicUser user) {
        String sql = "SELECT charity_id FROM charity_volunteers WHERE user_id = ?";
        try {
            return jdbcTemplate.query(sql, (resultSet, i) -> (
                    charityRepository.
                            getCharity(resultSet.getInt("charity_id"))), user.getId());
        }
        catch(Exception e){
            return new ArrayList<>();
        }
    }

    @Override
    public List<BasicUser> getVolunteersForCharity(Charity charity) {
        String sql = "SELECT user_id FROM charity_volunteers WHERE charity_id = ?";
        try {
            return jdbcTemplate.query(sql, (resultSet, i) -> (
                    basicUserRepository.
                            getUserById(resultSet.getInt("user_id"))), charity.getId());
        }
        catch(Exception e){
            return new ArrayList<>();
        }
    }

    @Override
    public void addVolunteerForCharity(CharityVolunteer charityVolunteer){
        String sql = "INSERT INTO charity_volunteers (charity_id, user_id) VALUES (?, ?)";
        jdbcTemplate.update(sql, charityVolunteer.getCharity().getId(), charityVolunteer.getUser().getId());
    }

}
