package com.example.finaltask.repositories;

import com.example.finaltask.models.User;

public interface IUserRepository {
//    List<User> getAllUsers();

    User getUserById(int id);

    User getUserByUsername(String username);

    void addUser(User user);

    void updateUser(int id, User user);

//    void deleteUser(int id);
}
