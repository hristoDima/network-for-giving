package com.example.finaltask.repositories;

import com.example.finaltask.models.BasicUser;
import com.example.finaltask.models.Charity;
import com.example.finaltask.models.Image;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class CharityRepository implements ICharityRepository {

    private JdbcTemplate jdbcTemplate;
    private BasicUserRepository basicUserRepository;
    private ImageRepository imageRepository;

    public CharityRepository(JdbcTemplate jdbcTemplate, BasicUserRepository basicUserRepository, ImageRepository imageRepository) {
        this.jdbcTemplate = jdbcTemplate;
        this.basicUserRepository = basicUserRepository;
        this.imageRepository = imageRepository;
    }

    @Override
    public List<Charity> getAllCharities() {
        String sql = "SELECT * FROM charity";
        return jdbcTemplate.query(sql, this::rowCharityMapper);
    }

    @Override
    public Charity getCharity(int id) {
        String sql = "SELECT * FROM charity WHERE id=?";
        return jdbcTemplate.queryForObject(sql, this::rowCharityMapper, id);
    }

    @Override
    public void addCharity(Charity charity) {
        String sql = "INSERT INTO " +
                "charity(name, thumbnail, description, budget_required, participants_required ,creator_id)" +
                "VALUES(?, ?, ?, ?, ?, ?)";
        jdbcTemplate.update(sql, charity.getName(), charity.getThumbnail(), charity.getDescription(), charity.getBudgetRequired(), charity.getParticipantsRequired(), charity.getCreator().getId());
    }

    @Override
    public void updateCharity(int id, Charity charity) {
        String sql = "UPDATE charity SET name=?, thumbnail=?, description=?, budget_required=?, participants_required=?, creator_id=? WHERE id=?";
        jdbcTemplate.update(sql, charity.getName(), charity.getThumbnail(), charity.getDescription(), charity.getBudgetRequired(), charity.getParticipantsRequired(), charity.getCreator().getId(), charity.getId());
    }

    @Override
    public void deleteCharity(int id) {
        String sql = "DELETE FROM charity WHERE id = ?";
        jdbcTemplate.update(sql, id);
    }

    private Charity rowCharityMapper(ResultSet rs, int rowsNum) throws SQLException {
        int id = rs.getInt("id");
        String name = rs.getString("name");
        String thumbnail = rs.getString("thumbnail");
        String description = rs.getString("description");
        double budget = rs.getDouble("budget_required");
        int participants = rs.getInt("participants_required");
        int creatorId = rs.getInt("creator_id");

        BasicUser creator = basicUserRepository.getUserById(creatorId);

        return new Charity(id, name, thumbnail, description, budget, participants, creator);
    }
}
