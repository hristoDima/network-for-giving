package com.example.finaltask.repositories;

import com.example.finaltask.models.Image;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;

@Repository
public class ImageRepository {
    JdbcTemplate jdbcTemplate;

    public ImageRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void addImage(Image image) {
        String sql = "INSERT INTO image(name, type, picByte ) VALUES (?, ?, ?)";
        this.jdbcTemplate.update(sql, image.getName(), image.getType(), image.getPicByte());
    }

    public Image getImageByName(String name) {
        String sql = "SELECT id, name, type, picbyte FROM image WHERE name=?";
        try {
            return this.jdbcTemplate.queryForObject(sql, this::rowImageMapper, name);
        }
        catch(Exception e){
            return null;
        }
    }

    public Image getImageById(int id) {
        String sql = "SELECT id, name, type, picbyte FROM image WHERE id=?";
        try {
            return this.jdbcTemplate.queryForObject(sql, this::rowImageMapper, id);
        }
        catch(Exception e){
            return null;
        }
    }

    private Image rowImageMapper(ResultSet rs, int rowsNum) throws SQLException {
        int id = rs.getInt("id");
        String name = rs.getString("name");
        String type = rs.getString("type");
        byte[] picByte = rs.getBytes("picbyte");
        return new Image(id, name, type, picByte);
    }
}
