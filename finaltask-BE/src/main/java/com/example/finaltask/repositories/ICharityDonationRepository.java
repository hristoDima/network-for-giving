package com.example.finaltask.repositories;

import com.example.finaltask.models.BasicUser;
import com.example.finaltask.models.Charity;
import com.example.finaltask.models.CharityDonation;

import java.util.List;

public interface ICharityDonationRepository {
    List<CharityDonation> getAllCharityDonationsForUser(BasicUser user);

    List<Double> getAllDonationsForCharity(Charity charity);

    List<Double> getAllDonationsAmountForUser(BasicUser user);

    List<CharityDonation> getAllDonatonsForCharity(Charity charity);

    void addNewDonation(CharityDonation charityDonation);
}
