package com.example.finaltask.repositories;

import com.example.finaltask.models.BasicUser;
import com.example.finaltask.models.Charity;
import com.example.finaltask.models.CharityDonation;
import com.example.finaltask.models.User;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class CharityDonationRepository implements ICharityDonationRepository {
    private JdbcTemplate jdbcTemplate;
    private IUserRepository userRepository;
    private ICharityRepository charityRepository;

    public CharityDonationRepository(JdbcTemplate jdbcTemplate, IUserRepository userRepository, ICharityRepository charityRepository) {
        this.jdbcTemplate = jdbcTemplate;
        this.userRepository = userRepository;
        this.charityRepository = charityRepository;
    }

    @Override
    public List<CharityDonation> getAllCharityDonationsForUser(BasicUser user) {
        String sql = "SELECT id, charity_id, amount_donated FROM charity_donations where user_id = ? ORDER BY amount_donated";
        try {
            return this.jdbcTemplate.query(sql, (resultSet, i) -> {
                int donationId = resultSet.getInt("id");
                int charityId = resultSet.getInt("charity_id");
                double donation = resultSet.getDouble("amount_donated");

                Charity charity = charityRepository.getCharity(charityId);

                return new CharityDonation(donationId, user, charity, donation);
            }, user.getId());
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    @Override
    public List<Double> getAllDonationsForCharity(Charity charity) {
        String sql = "SELECT amount_donated FROM charity_donations where charity_id = ? ORDER BY amount_donated";
        try {
            return this.jdbcTemplate.queryForList(sql, Double.class, charity.getId());
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    @Override
    public List<Double> getAllDonationsAmountForUser(BasicUser user) {
        String sql = "SELECT amount_donated FROM charity_donations where user_id = ? ORDER BY amount_donated";
        try {
            return this.jdbcTemplate.queryForList(sql, Double.class, user.getId());
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    @Override
    public List<CharityDonation> getAllDonatonsForCharity(Charity charity) {
        String sql = "SELECT id, user_id, amount_donated FROM charity_donations where charity_id = ? ORDER BY amount_donated";
        try {
            return this.jdbcTemplate.query(sql, (resultSet, i) -> {
                int donationId = resultSet.getInt("id");
                int userId = resultSet.getInt("user_id");
                double donation = resultSet.getDouble("amount_donated");

                User user = userRepository.getUserById(userId);

                return new CharityDonation(donationId, user, charity, donation);
            }, charity.getId());
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    @Override
    public void addNewDonation(CharityDonation charityDonation) {
        String sql = "INSERT INTO charity_donations (user_id, charity_id, amount_donated) VALUES (?, ?, ?)";
        jdbcTemplate.update(sql, charityDonation.getUser().getId(), charityDonation.getCharity().getId(), charityDonation.getDonation());
    }
}
