package com.example.finaltask.repositories;

import com.example.finaltask.models.Charity;

import java.util.List;

public interface ICharityRepository {
    public List<Charity> getAllCharities();

    public Charity getCharity(int id);

    public void addCharity(Charity charity);

    public void updateCharity(int id, Charity charity);

    public void deleteCharity(int id);
}
