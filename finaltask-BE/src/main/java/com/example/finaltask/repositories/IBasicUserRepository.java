package com.example.finaltask.repositories;

import com.example.finaltask.models.BasicUser;

public interface IBasicUserRepository {
    BasicUser getUserById(int id);
    BasicUser getUserByUsername(String username);
}
