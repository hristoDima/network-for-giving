package com.example.finaltask.repositories;

import com.example.finaltask.models.BasicUser;
import com.example.finaltask.models.Charity;
import com.example.finaltask.models.CharityVolunteer;
import com.example.finaltask.models.User;

import java.util.List;

public interface ICharityVolunteerRepository {
    List<Charity> getCharitiesForUser(BasicUser user);

    List<BasicUser> getVolunteersForCharity(Charity charity);

    void addVolunteerForCharity(CharityVolunteer charityVolunteer);

}
