package com.example.finaltask.repositories;

import com.example.finaltask.models.BasicUser;
import com.example.finaltask.utillity.ExceptionLoggerUtil;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;

@Repository
public class BasicUserRepository implements IBasicUserRepository {
    private JdbcTemplate jdbcTemplate;

    public BasicUserRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public BasicUser getUserById(int id) {
        String sql = "SELECT * FROM user WHERE user.id=? ";
        try {
            return jdbcTemplate.queryForObject(sql, this::mapUserRow, id);
        } catch (DataAccessException e) {
            ExceptionLoggerUtil.selectQueryLog(sql, e);
            return null;
        }
    }

    @Override
    public BasicUser getUserByUsername(String username) {
        String sql = "SELECT * FROM user WHERE user.username=?";
        try {
            return jdbcTemplate.queryForObject(sql, this::mapUserRow, username);
        } catch (DataAccessException e) {
            ExceptionLoggerUtil.selectQueryLog(sql, e);
            return null;
        }

    }

    private BasicUser mapUserRow(ResultSet rs, int rowNum) throws SQLException {
        int id = rs.getInt("id");
        String firstName = rs.getString("fname");
        String lastName = rs.getString("lname");
        String gender = rs.getString("gender");
        int age = rs.getInt("age");
        String location = rs.getString("location");
        String username = rs.getString("username");

        BasicUser user = new BasicUser(id, firstName, lastName, gender, age, location, username);
        return user;
    }

}
