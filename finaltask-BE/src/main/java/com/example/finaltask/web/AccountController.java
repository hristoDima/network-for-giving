package com.example.finaltask.web;

import com.example.finaltask.exceptions.UserAlreadyExistsException;
import com.example.finaltask.models.BasicUser;
import com.example.finaltask.models.User;
import com.example.finaltask.services.IUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
public class AccountController {

    private final IUserService userService;

    public AccountController(IUserService userService) {
        this.userService = userService;
    }

    @PostMapping("/registration")
    public ResponseEntity register(@RequestBody @Valid User user) {
        try {
            userService.registerNewUser(user);
            return ResponseEntity.ok().body("User created");
        } catch (UserAlreadyExistsException e) {
            log.info("User exists exception: ", e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("User already exists");
        }
    }

    @GetMapping("/user")
    public ResponseEntity<BasicUser> principalInformation(Principal principal) {
        BasicUser user = null;
        if (principal == null) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(user);
        }
        user = userService.getUserByUsername(principal.getName());
        return ResponseEntity.ok().body(user);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();

        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }

//
//    @GetMapping("/account/{id}")
//    public User getAccountInfo(@PathVariable("id") int id){
//        return userService.getUser(id);
//    }
//
//    @DeleteMapping("/account/{id}")
//    public void deleteAccount(@PathVariable("id") int id){
//        userService.deleteUser(id);
//    }

}
