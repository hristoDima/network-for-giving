package com.example.finaltask.web;

import com.example.finaltask.models.BasicUser;
import com.example.finaltask.models.Charity;
import com.example.finaltask.models.CharityDonation;
import com.example.finaltask.models.CharityVolunteer;
import com.example.finaltask.services.ICharityService;
import com.example.finaltask.services.IUserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.coyote.Response;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.security.Principal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
public class CharityController {

    private final ICharityService charityService;
    private final IUserService userService;

    public CharityController(ICharityService charityService, IUserService userService) {
        this.charityService = charityService;
        this.userService = userService;
    }

    @GetMapping(value = "/charity")
    public ResponseEntity<List<Charity>> getCharities() {
        List<Charity> charities = null;
        try {
            charities = charityService.getAllCharities();
            if (charities != null)
                return ResponseEntity.ok().body(charities);
            else
                return ResponseEntity.status(HttpStatus.NO_CONTENT).body(charities);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(charities);
        }
    }

    @PostMapping(value = "/charity/create", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity createCharity(@RequestBody @Valid Charity charity, Principal principal) {
        if (principal == null) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("");
        }
        try {
            BasicUser creator = userService.getUserByUsername(principal.getName());
            charityService.addNewCharity(charity, creator);
            return ResponseEntity.ok().body("");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("");
        }
    }


    @GetMapping("/user/activity")
    public ResponseEntity<Map<String, Object>> getUserActivity(Principal principal) {
        Map<String, Object> activity = new HashMap<>();

        Double donatedMoney = null;
        List<CharityDonation> donations = new ArrayList<>();
        List<Charity> volunteerCharities = new ArrayList<>();

        if (principal == null) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(activity);
        }
        try {
            BasicUser user = userService.getUserByUsername(principal.getName());
            donations = userService.getDonationsForPrincipal(user);
            volunteerCharities = userService.getVolunteerCharitiesForPrincipal(user);
            donatedMoney = userService.getDonatedMoney(user);

            activity.put("volunteeredIn", volunteerCharities);
            activity.put("donatedTo", donations);
            activity.put("amountDonated", donatedMoney);

            return ResponseEntity.ok().body(activity);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(activity);
        }

    }


    @GetMapping(value = "/user/{username}")
    public ResponseEntity<Map<String, Object>> getOtherUserActivity(@PathVariable("username") String username, Principal principal) {

        Map<String, Object> charities = new HashMap<>();
        List<Charity> volunteerCharities = null;
        List<Charity> donationCharities = null;

        if (principal == null) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(charities);
        }
        try {
            BasicUser user = userService.getUserByUsername(username);
            if (user == null)
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(charities);

            volunteerCharities = userService.getVolunteerCharitiesForOtherUser(user);
            donationCharities = userService.getDonationsForOtherUser(user);
            charities.put("user", user);
            charities.put("volunteeredIn", volunteerCharities);
            charities.put("donatedTo", donationCharities);
            return ResponseEntity.ok(charities);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(charities);
        }
    }

    @GetMapping(value = "/charity/{id}")
    public ResponseEntity<Charity> getCharity(@PathVariable("id") int id, Principal principal) {
        Charity charity = null;

        if (principal == null) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(charity);
        }

        try {
            charity = charityService.getCharity(id);
            if (charity == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(charity);
            }
            return ResponseEntity.status(HttpStatus.OK).body(charity);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(charity);
        }
    }

    @GetMapping(value = "/charity/{id}/activity")
    public ResponseEntity<Map<String, Object>> getCharityActivity(@PathVariable("id") int id, Principal principal) {
        Map<String, Object> charityActivity = new HashMap<>();

        if (principal == null) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(charityActivity);
        }

        try {
            Charity charity = charityService.getCharity(id);

            if (charity == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(charityActivity);
            }

            List<BasicUser> donators = userService.getDonatorsForCharity(charity);
            List<BasicUser> volunteers = userService.getVolunteersForCharity(charity);

            charityActivity.put("donators", donators);
            charityActivity.put("volunteers", volunteers);

            return ResponseEntity.status(HttpStatus.OK).body(charityActivity);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(charityActivity);
        }
    }

    @PostMapping(value = "/charity/{id}/donate")
    public ResponseEntity donate(@PathVariable("id") int id, @RequestBody double donationAmount, Principal principal) {
        if (principal == null) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Session expired");
        }

        try {
            BasicUser user = userService.getUserByUsername(principal.getName());
            if (user == null) {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Session expired");
            }

            Charity charity = charityService.getCharity(id);
            if (charity == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Charity does not exists");
            }

            CharityDonation donation = new CharityDonation(user, charity, donationAmount);
            charityService.addDonation(donation);
            return ResponseEntity.ok("");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("");
        }
    }

    @PostMapping(value = "/charity/{id}/participate")
    public ResponseEntity join(@PathVariable("id") int id, Principal principal) {
        if (principal == null) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Session expired");
        }

        try {
            BasicUser user = userService.getUserByUsername(principal.getName());
            if (user == null) {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Session expired");
            }

            Charity charity = charityService.getCharity(id);
            if (charity == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Charity does not exists");
            }
            CharityVolunteer volunteer = new CharityVolunteer(charity, user);

            charityService.addVolunteer(volunteer);
            return ResponseEntity.ok("");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("");
        }
    }

    @GetMapping(value = "/placeholder/{id}/stats")
    public ResponseEntity<Map<String, Object>> getCharityActivity(@PathVariable("id") int id) {
        Map<String, Object> charityStats = new HashMap<>();

        try {
            Charity charity = charityService.getCharity(id);

            if (charity == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(charityStats);
            }

            double donationsAmount = charityService.getDonationsAmountForCharity(charity);
            int volunteersCount = charityService.getVolunteersCountForCharity(charity);

            charityStats.put("donations", donationsAmount);
            charityStats.put("volunteers", volunteersCount);

            return ResponseEntity.status(HttpStatus.OK).body(charityStats);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(charityStats);
        }
    }

}


