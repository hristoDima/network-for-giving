package com.example.finaltask.web;

import com.example.finaltask.models.Image;
import com.example.finaltask.services.ImageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Slf4j
@RestController
@RequestMapping(path = "image")
public class ImageController {
    @Autowired
    ImageService imageService;

    @PostMapping("/upload")
    public ResponseEntity uploadImage(@RequestParam("imageFile") MultipartFile file) throws IOException {
        try {
            imageService.uploadImage(file);
            return ResponseEntity.status(HttpStatus.OK).body("");
        }catch(Exception e){
            log.error("upload error", e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("");
        }
    }

    @GetMapping(path = { "/get/{imageName}" })
    public ResponseEntity<Image> getImage(@PathVariable("imageName") String imageName) throws IOException {
        Image img = null;
        try {
            img = imageService.getImage(imageName);
            return ResponseEntity.status(HttpStatus.OK).body(img);
        }
        catch(Exception e){
            log.error("Get image " + imageName + "error: ", e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(img);
        }
    }

}
