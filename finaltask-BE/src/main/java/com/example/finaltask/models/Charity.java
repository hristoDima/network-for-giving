package com.example.finaltask.models;

import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * CREATE TABLE charity(
 * id INT PRIMARY KEY AUTO_INCREMENT,
 * name VARCHAR(100) NOT NULL,
 * thumbnail VARCHAR(30) NOT NULL,
 * description VARCHAR(5000) NOT NULL,
 * budget_required DOUBLE NOT NULL DEFAULT 0,
 * participants_required INTEGER NOT NULL DEFAULT 0,
 * creator_id INT NOT NULL REFERENCES user(id)
 * );
 */

public class Charity {
    private int id;
    @NotBlank
    @Size(min=1, max=100)
    private String name;
    private String thumbnail;
    @NotBlank
    @Size(min=1, max=5000)
    private String description;
    @NotNull
    @Min(0)
    private double budgetRequired;
    @NotNull
    @Min(0)
    private int participantsRequired;
    private BasicUser creator;

    public Charity() {
    }

    public Charity(int id, String name, String thumbnail, String description, double budgetRequired, int participantsRequired, BasicUser creator) {
        this.id = id;
        this.name = name;
        this.thumbnail = thumbnail;
        this.description = description;
        this.budgetRequired = budgetRequired;
        this.participantsRequired = participantsRequired;
        this.creator = creator;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getBudgetRequired() {
        return budgetRequired;
    }

    public void setBudgetRequired(double budgetRequired) {
        this.budgetRequired = budgetRequired;
    }

    public int getParticipantsRequired() {
        return participantsRequired;
    }

    public void setParticipantsRequired(int participantsRequired) {
        this.participantsRequired = participantsRequired;
    }

    public BasicUser getCreator() {
        return creator;
    }

    public void setCreator(BasicUser creator) {
        this.creator = creator;
    }
}
