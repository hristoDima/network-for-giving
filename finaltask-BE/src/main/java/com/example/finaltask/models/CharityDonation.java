package com.example.finaltask.models;

public class CharityDonation {
    private int id;
    private BasicUser user;
    private Charity charity;
    private double donation;

    public CharityDonation(){
    };

    public CharityDonation(int id, BasicUser user, Charity charity, double donation) {
        this.id = id;
        this.user = user;
        this.charity = charity;
        this.donation = donation;
    }

    public CharityDonation(BasicUser user, Charity charity, double donation) {
        this.user = user;
        this.charity = charity;
        this.donation = donation;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public BasicUser getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Charity getCharity() {
        return charity;
    }

    public void setCharity(Charity charity) {
        this.charity = charity;
    }

    public double getDonation() {
        return donation;
    }

    public void setDonation(double donation) {
        this.donation = donation;
    }
}
