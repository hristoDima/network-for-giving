package com.example.finaltask.models;

import javax.validation.constraints.*;

public class BasicUser {
    private int id;
    @NotBlank(message = "Last name should not be blank!")
    @Size(min = 1, max = 20, message = "Incorrect first name length! It must be between 1 and 20 characters!")
    private String fname;
    @NotBlank(message = "Last name should not be blank!")
    @Size(min = 1, max = 20, message = "Incorrect last name length! It must be between 1 and 20 characters!")
    private String lname;
    @Size(max = 1)
    private String gender;
    @NotNull(message = "Age name should not be blank!")
    @Min(value = 1, message = "Age must be a positive number!")
    @Max(value = 150, message = "You can't be that old!")
    private int age;
    @Size(max = 100, message = "Location size too long!")
    private String location;
    @NotBlank(message = "Username can't be blank!")
    @Size(min = 1, max = 30, message = "Username must be between 1 and 30 characters!")
    private String username;

    public BasicUser() {
    }

    public BasicUser(int id, String fname, String lname, String gender, int age, String location, String username, String password) {
        this.id = id;
        this.fname = fname;
        this.lname = lname;
        this.gender = gender;
        this.age = age;
        this.location = location;
        this.username = username;
    }

    public BasicUser(int id, String fname, String lname, String gender, int age, String location, String username) {
        this.id = id;
        this.fname = fname;
        this.lname = lname;
        this.gender = gender;
        this.age = age;
        this.location = location;
        this.username = username;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
