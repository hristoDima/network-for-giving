package com.example.finaltask.models;

public class CharityVolunteer {
    private int id;
    private Charity charity;
    private BasicUser user;

    public CharityVolunteer() {
    }

    public CharityVolunteer(int id, Charity charity, BasicUser user) {
        this.id = id;
        this.charity = charity;
        this.user = user;
    }

    public CharityVolunteer(Charity charity, BasicUser user) {
        this.charity = charity;
        this.user = user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Charity getCharity() {
        return charity;
    }

    public void setCharity(Charity charity) {
        this.charity = charity;
    }

    public BasicUser getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
