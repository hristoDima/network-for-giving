package com.example.finaltask.models;

import javax.validation.constraints.*;

/**
 * CREATE TABLE user(
 * id INT PRIMARY KEY AUTO_INCREMENT,
 * fname VARCHAR(20) NOT NULL,
 * lname VARCHAR(20) NOT NULL,
 * gender CHAR(1) NULL,
 * age INTEGER NULL,
 * location VARCHAR(100) NULL,
 * username VARCHAR(30) NOT NULL UNIQUE,
 * password VARCHAR(50) NOT NULL,
 * );
 */
public class User extends BasicUser{

    @NotBlank(message = "Password must not be blank!")
    @Size(min = 5, max = 30, message="password must be between 5 and 30 symbols!")
    private String password;

    public User() {
    }

    public User(int id, String fname, String lname, String gender, int age, String location, String username, String password) {
        super(id,fname, lname, gender, age, location, username);
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
