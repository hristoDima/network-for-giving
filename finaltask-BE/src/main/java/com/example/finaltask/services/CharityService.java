package com.example.finaltask.services;

import com.example.finaltask.models.BasicUser;
import com.example.finaltask.models.Charity;
import com.example.finaltask.models.CharityDonation;
import com.example.finaltask.models.CharityVolunteer;
import com.example.finaltask.repositories.ICharityDonationRepository;
import com.example.finaltask.repositories.ICharityRepository;
import com.example.finaltask.repositories.ICharityVolunteerRepository;
import com.example.finaltask.repositories.IUserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@Slf4j
@Service
public class CharityService implements ICharityService {

    private ICharityRepository charityRepository;
    private IUserRepository userRepository;
    private ICharityDonationRepository charityDonationRepository;
    private ICharityVolunteerRepository charityVolunteerRepository;

    private final Path root = Paths.get("uploads");

    public CharityService(ICharityRepository charityRepository, IUserRepository userRepository, ICharityDonationRepository charityDonationRepository, ICharityVolunteerRepository charityVolunteerRepository) {
        this.charityRepository = charityRepository;
        this.userRepository = userRepository;
        this.charityDonationRepository = charityDonationRepository;
        this.charityVolunteerRepository = charityVolunteerRepository;

    }

    @Override
    public List<Charity> getAllCharities() {
        return charityRepository.getAllCharities();
    }

    @Override
    public Charity getCharity(int id) {
        return charityRepository.getCharity(id);
    }

    @Override
    public void addNewCharity(Charity charity, BasicUser creator) {
        charity.setCreator(creator);
        charityRepository.addCharity(charity);
    }

    @Override
    public void updateCharity(int id, Charity charity) {
        charityRepository.updateCharity(id, charity);
    }

    @Override
    public void deleteCharity(int id) {
        charityRepository.deleteCharity(id);
    }

    @Override
    public void addDonation(CharityDonation donation) {
        this.charityDonationRepository.addNewDonation(donation);
    }

    @Override
    public void addVolunteer(CharityVolunteer charityVolunteer) {
        this.charityVolunteerRepository.addVolunteerForCharity(charityVolunteer);
    }

    @Override
    public double getDonationsAmountForCharity(Charity charity) {
        List<CharityDonation> donations = charityDonationRepository.getAllDonatonsForCharity(charity);
        double sum = 0;
        for(CharityDonation donation: donations){
            sum += donation.getDonation();
        }
        return sum;
    }

    @Override
    public int getVolunteersCountForCharity(Charity charity) {
        List<BasicUser> volunteers = charityVolunteerRepository.getVolunteersForCharity(charity);
        return volunteers.size();
    }
}
