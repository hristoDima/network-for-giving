package com.example.finaltask.services;

import com.example.finaltask.exceptions.UserAlreadyExistsException;
import com.example.finaltask.models.BasicUser;
import com.example.finaltask.models.Charity;
import com.example.finaltask.models.CharityDonation;
import com.example.finaltask.models.User;

import java.util.List;

public interface IUserService {
//    List<User> getAllUsers();

    BasicUser getUser(int id);

    BasicUser getUserByUsername(String username);

    void registerNewUser(User user) throws UserAlreadyExistsException;

    void updateUserInfo(int id, User user);

//    void deleteUser(int id);

    double getDonatedMoney(BasicUser user);

    List<Charity> getVolunteerCharitiesForPrincipal(BasicUser user);

    List<Charity> getVolunteerCharitiesForOtherUser(BasicUser user);

    List<CharityDonation> getDonationsForPrincipal(BasicUser user);

    List<Charity> getDonationsForOtherUser(BasicUser user);

    List<BasicUser> getVolunteersForCharity(Charity charity);

    List<BasicUser> getDonatorsForCharity(Charity charity);

}
