package com.example.finaltask.services;

import com.example.finaltask.exceptions.UserAlreadyExistsException;
import com.example.finaltask.models.BasicUser;
import com.example.finaltask.models.Charity;
import com.example.finaltask.models.CharityDonation;
import com.example.finaltask.models.User;
import com.example.finaltask.repositories.IBasicUserRepository;
import com.example.finaltask.repositories.ICharityDonationRepository;
import com.example.finaltask.repositories.ICharityVolunteerRepository;
import com.example.finaltask.repositories.IUserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class UserService implements IUserService {

    private IUserRepository userRepository;
    private IBasicUserRepository basicUserRepository;
    private ICharityVolunteerRepository charityVolunteerRepository;
    private ICharityDonationRepository charityDonationRepository;

    private PasswordEncoder passwordEncoder;

    public UserService(IUserRepository userRepository, IBasicUserRepository basicUserRepository, ICharityVolunteerRepository charityVolunteerRepository, ICharityDonationRepository charityDonationRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.basicUserRepository = basicUserRepository;
        this.charityVolunteerRepository = charityVolunteerRepository;
        this.charityDonationRepository = charityDonationRepository;
        this.passwordEncoder = passwordEncoder;
    }

//    @Override
//    public List<User> getAllUsers() {
//        List<User> users = userRepository.getAllUsers();
//        for(User user : users){
//            removePass(user);
//        }
//        return users;
//    }

    @Override
    public BasicUser getUser(int id) {
        return basicUserRepository.getUserById(id);
    }

    @Override
    public BasicUser getUserByUsername(String username) {
        return basicUserRepository.getUserByUsername(username);
    }

    @Override
    public void registerNewUser(User user) throws UserAlreadyExistsException {
        if (usernameExists(user.getUsername())) {
            throw new UserAlreadyExistsException("There is already user with the username: " + user.getUsername());
        }
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userRepository.addUser(user);
        log.info("new user registered with username " + user.getUsername());
    }

    private boolean usernameExists(String username) {
        return this.userRepository.getUserByUsername(username) != null;
    }

    @Override
    public void updateUserInfo(int id, User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        this.userRepository.updateUser(id, user);
    }

//    @Override
//    public void deleteUser(int id) {
//        this.userRepository.deleteUser(id);
//    }

    @Override
    public double getDonatedMoney(BasicUser user) {
        List<Double> donations = charityDonationRepository.getAllDonationsAmountForUser(user);
        double sum = 0;
        for (double donation : donations) {
            sum += donation;
        }
        return sum;
    }

    @Override
    public List<Charity> getVolunteerCharitiesForPrincipal(BasicUser user) {
        return charityVolunteerRepository.getCharitiesForUser(user);
    }

    @Override
    public List<Charity> getVolunteerCharitiesForOtherUser(BasicUser user) {
        return charityVolunteerRepository.getCharitiesForUser(user);
    }

    @Override
    public List<CharityDonation> getDonationsForPrincipal(BasicUser user) {
        return charityDonationRepository.getAllCharityDonationsForUser(user);
    }

    @Override
    public List<Charity> getDonationsForOtherUser(BasicUser user) {
        List<CharityDonation> charityDonations = charityDonationRepository.getAllCharityDonationsForUser(user);
        List<Charity> charities = new ArrayList<>();
        for (CharityDonation donation : charityDonations) {
            charities.add(donation.getCharity());
        }
        return charities;
    }

    @Override
    public List<BasicUser> getVolunteersForCharity(Charity charity) {
        return charityVolunteerRepository.getVolunteersForCharity(charity);
    }

    @Override
    public List<BasicUser> getDonatorsForCharity(Charity charity) {
        List<CharityDonation> donations = charityDonationRepository.getAllDonatonsForCharity(charity);
        List<BasicUser> donators = new ArrayList<>();

        for(CharityDonation donation: donations){
            donators.add(donation.getUser());
        }

        return donators;
    }


}
