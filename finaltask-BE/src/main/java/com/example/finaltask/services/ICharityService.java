package com.example.finaltask.services;

import com.example.finaltask.models.BasicUser;
import com.example.finaltask.models.Charity;
import com.example.finaltask.models.CharityDonation;
import com.example.finaltask.models.CharityVolunteer;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface ICharityService {
    List<Charity> getAllCharities();

    Charity getCharity(int id);

    void addNewCharity(Charity charity, BasicUser creator);

    void updateCharity(int id, Charity charity);

    void deleteCharity(int id);

    void addDonation(CharityDonation donation);

    void addVolunteer(CharityVolunteer charityVolunteer);

    double getDonationsAmountForCharity(Charity charity);

    int getVolunteersCountForCharity(Charity charity);
}
