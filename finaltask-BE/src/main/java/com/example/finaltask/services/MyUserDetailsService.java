package com.example.finaltask.services;

import com.example.finaltask.models.User;
import com.example.finaltask.repositories.IUserRepository;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashSet;

@Service
public class MyUserDetailsService implements UserDetailsService {
    private final IUserRepository userRepository;

    public MyUserDetailsService(IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.getUserByUsername(username);
        if(user == null){
            throw new UsernameNotFoundException("No user found with username " + username);
        }

        boolean enabled = true;
        boolean accountNonExpired = true;
        boolean credentialsNonExpired = true;
        boolean accountNonLocked = true;

//        return new MyUser(user.getUsername(), user.getPassword(), enabled, accountNonExpired,
//                credentialsNonExpired, accountNonLocked, new HashSet<GrantedAuthority>(),
//                user.getFname(), user.getLname(), user.getLocation());

        return new org.springframework.security.core.userdetails.User(
                user.getUsername(), user.getPassword(), enabled, accountNonExpired,
                credentialsNonExpired, accountNonLocked, new HashSet<GrantedAuthority>());
    }

//    public class MyUser extends org.springframework.security.core.userdetails.User{
//        private final String fname;
//        private final String lname;
//        private final String location;
//
//        public MyUser(String username, String password,
//                      Collection<? extends GrantedAuthority> authorities,
//                      String fname, String lname, String location) {
//            super(username, password, authorities);
//            this.fname = fname;
//            this.lname = lname;
//            this.location = location;
//        }
//
//        public MyUser(String username, String password, boolean enabled, boolean accountNonExpired,
//                      boolean credentialsNonExpired, boolean accountNonLocked,
//                      Collection<? extends GrantedAuthority> authorities, String fname, String lname, String location) {
//            super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
//            this.fname = fname;
//            this.lname = lname;
//            this.location = location;
//        }
//
//        public String getFname() {
//            return fname;
//        }
//
//        public String getLname() {
//            return lname;
//        }
//
//        public String getLocation() {
//            return location;
//        }
//    }
}
