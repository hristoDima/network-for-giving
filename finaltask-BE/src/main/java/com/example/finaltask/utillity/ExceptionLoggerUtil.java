package com.example.finaltask.utillity;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ExceptionLoggerUtil {
    public static void selectQueryLog(String query, Exception e){
        log.error("Exception from query " + query , e);
    }
}
