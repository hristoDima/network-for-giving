export class User {
    id: number;
    fname: string;
    lname: string;
    gender: string;
    age: number;
    location: string;
    username: string;
    password: string;

    public constructor(init?: Partial<User>){
        Object.assign(this, init);
    }
}
