import {User} from './user';
import {Charity} from './charity';

export interface CharityDonation {
    user: User;
    charity: Charity;
    donation: number;
}
