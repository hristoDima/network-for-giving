import {User} from './user';

export class Charity {
    id: number;
    name: string;
    thumbnail: string;
    description: string;
    budgetRequired: number;
    participantsRequired: number;
    creator: User;

    public constructor(init?: Partial<Charity>){
    Object.assign(this, init);
}
}
