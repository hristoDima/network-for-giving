import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class UserActionService {

    baseUrl = 'http://localhost:8080';

    constructor(private http: HttpClient) {
    }

    donate(id: number, donationAmount: number): Observable<any>{
        const donateUrl = this.baseUrl + `/charity/${id}/donate`;
        return this.http.post<any>(donateUrl, donationAmount);
    }

    participate(id: number): Observable<any> {
        const participateUrl = this.baseUrl + `/charity/${id}/participate`;
        // return new Observable();
        return this.http.post<any>(participateUrl, []);
    }
}
