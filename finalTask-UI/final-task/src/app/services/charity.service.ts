import {Injectable} from '@angular/core';

import {Charity} from '../../models/charity';

import {Observable, of} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, map, tap} from 'rxjs/operators';
import {User} from '../../models/user';

@Injectable({
    providedIn: 'root'
})
export class CharityService {
    private baseUrl = 'http://localhost:8080';


    constructor(private http: HttpClient) {
    }

    getAllCharities(): Observable<Charity[]> {
        const getAllUrl = this.baseUrl + '/charity';
        return this.http.get<Charity[]>(getAllUrl);
    }

    addCharity(charity: Charity): Observable<any> {
        const uploadUrl = this.baseUrl + '/charity/create';
        return this.http.post<any>(uploadUrl, charity);
    }

    uploadThumbnail(formData: FormData): Observable<any> {
        const uploadUrl = this.baseUrl + '/image/upload';
        return this.http.post<any>(uploadUrl, formData);
    }

    getCharity(id: number): Observable<Charity> {
        const charityUrl = this.baseUrl + `/charity/${id}`;
        return this.http.get<Charity>(charityUrl);
    }

    getCharityActivity(id: number): Observable<CharityActivity> {
        const charityActivityUrl = this.baseUrl + `/charity/${id}/activity`;
        return this.http.get<CharityActivity>(charityActivityUrl);
    }

    getThumbnailImage(imageName: string): Observable<any> {
        const imageUrl = this.baseUrl + '/image/get/' + imageName;
        return this.http.get<any>(imageUrl);
    }

    getCharityStats(id: number): Observable<CharityStats> {
        const url = this.baseUrl + `/placeholder/${id}/stats`;
        return this.http.get<CharityStats>(url);
    }
}

export interface CharityActivity {
    donators: User[];
    volunteers: User[];
}

export interface CharityStats {
    donations: number;
    volunteers: number;
}
