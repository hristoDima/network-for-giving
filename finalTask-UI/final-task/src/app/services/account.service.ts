import {Injectable} from '@angular/core';

import {Observable, of} from 'rxjs';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import {catchError, map, tap} from 'rxjs/operators';
import {User} from '../../models/user';
import {CookieService} from 'ngx-cookie-service';
import {Charity} from '../../models/charity';
import {CharityDonation} from '../../models/charityDonation';
import {Router} from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class AccountService {
    private baseUrl = 'http://localhost:8080';

    constructor(private http: HttpClient,
                private cookieService: CookieService,
                private router: Router) {
    }

    registerUser(user: User): Observable<any> {
        const registrationUrl = this.baseUrl + '/registration';
        return this.http.post<any>(registrationUrl, user);
    }

    getUserDetails(): Observable<User> {
        const userUrl = this.baseUrl + '/user';
        return this.http.get<User>(userUrl);
    }

    getUserActivity(): Observable<UserActivity> {
        const userActivityUrl = this.baseUrl + '/user/activity';
        return this.http.get<UserActivity>(userActivityUrl);
    }

}

export interface UserActivity {
    volunteeredIn: Charity[];
    donatedTo: CharityDonation[];
    amountDonated: number;
}
