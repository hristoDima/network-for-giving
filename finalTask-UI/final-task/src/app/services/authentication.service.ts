import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CookieService} from 'ngx-cookie-service';

import {User} from '../../models/user';
import {Observable} from 'rxjs';
import {filter, pairwise, tap} from 'rxjs/operators';
import {Router, RoutesRecognized} from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class AuthenticationService {

    private baseUrl = 'http://localhost:8080';

    private isUserAuthenticated: boolean;

    private previousUrl: string;

    constructor(private http: HttpClient,
                private cookieService: CookieService,
                private router: Router) {
        this.isUserAuthenticated = this.cookieService.check('username');

        this.router.events
            .pipe(filter((evt: any) => evt instanceof RoutesRecognized), pairwise())
            .subscribe((events: RoutesRecognized[]) => {
                this.previousUrl = events[0].urlAfterRedirects;
            });
    }

    authenticate(userCredentials): Observable<any> {
        const loginUrl = this.baseUrl + '/login' + `?username=${userCredentials.username}&password=${userCredentials.password}`;
        return this.http.post<any>(loginUrl, userCredentials).pipe(
            tap(userInfo => {
                this.cookieService.set('username', userInfo.username);
                this.isUserAuthenticated = this.cookieService.check('username');
                this.router.navigate([this.previousUrl]);
            })
        );
    }

    logout(): Observable<any> {
        const logoutUrl = this.baseUrl + '/logout';
        return this.http.get<any>(logoutUrl).pipe(
            tap(_ => {
                this.cookieService.delete('username');
                this.isUserAuthenticated = this.cookieService.check('username');
            }));
    }

    isAuthenticated(): boolean {
        return this.isUserAuthenticated;
    }
}
