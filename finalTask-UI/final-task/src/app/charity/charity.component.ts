import {Component, OnInit} from '@angular/core';

import {Charity} from '../../models/charity';
import {CharityService} from '../services/charity.service';

@Component({
    selector: 'app-charity',
    templateUrl: './charity.component.html',
    styleUrls: ['./charity.component.css']
})
export class CharityComponent implements OnInit {

    charities: Charity[];

    constructor(private charityService: CharityService) {
    }

    ngOnInit(): void {
        this.charityService.getAllCharities().subscribe(charities => {
            this.charities = charities;
        });
    }

}
