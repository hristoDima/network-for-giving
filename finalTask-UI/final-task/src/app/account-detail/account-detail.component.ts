import {Component, OnInit} from '@angular/core';

import {AccountService, UserActivity} from '../services/account.service';
import {User} from '../../models/user';

@Component({
    selector: 'app-account-detail',
    templateUrl: './account-detail.component.html',
    styleUrls: ['./account-detail.component.css']
})
export class AccountDetailComponent implements OnInit {
    public user: User;
    public userActivity: UserActivity;

    constructor(private accountService: AccountService) {
    }

    ngOnInit(): void {
        this.accountService.getUserDetails().subscribe(user => {
            this.user = user;
        });
        this.accountService.getUserActivity().subscribe(userActivity => {
            this.userActivity = userActivity;
        });
    }

}
