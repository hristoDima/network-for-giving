import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

import {AccountService} from '../services/account.service';
import {FormControl, FormGroup} from '@angular/forms';
import {AuthenticationService} from '../services/authentication.service';


@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    loginForm = new FormGroup({
        username: new FormControl(''),
        password: new FormControl('')
    });

    constructor(private authenticationService: AuthenticationService,
                private router: Router) {
    }

    ngOnInit(): void {
    }

    submit() {
        this.authenticationService.authenticate(this.loginForm.value).subscribe();
    }

}
