import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router, RoutesRecognized} from '@angular/router';
import {Observable} from 'rxjs';
import {AuthenticationService} from './services/authentication.service';
import {filter, pairwise} from 'rxjs/operators';
import {state} from '@angular/animations';

@Injectable({
    providedIn: 'root'
})
export class LoggedGuard implements CanActivate {


    constructor(private router: Router,
                private authenticationService: AuthenticationService
    ) {
    }

    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot)
        : Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

        const currentUser = this.authenticationService.isAuthenticated();

        if (currentUser) {
            // if the user is logged in and tries to access login or registration page - log him out and redirect
            this.authenticationService.logout().subscribe(_ => {
                this.router.navigate(['/login']);
            });
            return false;
        }
        return true;
    }
}
