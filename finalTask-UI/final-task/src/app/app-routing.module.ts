import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {RegistrationComponent} from './registration/registration.component';
import {LoginComponent} from './login/login.component';
import {HomePageComponent} from './home-page/home-page.component';
import {CharityComponent} from './charity/charity.component';
import {CharityDetailComponent} from './charity-detail/charity-detail.component';
import {AccountDetailComponent} from './account-detail/account-detail.component';
import {CharityCreateComponent} from './charity-create/charity-create.component';
import {AuthGuard} from './auth.guard';
import {LoggedGuard} from './logged.guard';

const routes: Routes = [
    {path: '', component: HomePageComponent},
    {path: 'registration', component: RegistrationComponent, canActivate: [LoggedGuard]},
    {path: 'login', component: LoginComponent, canActivate: [LoggedGuard]},
    {path: 'account', component: AccountDetailComponent, canActivate: [AuthGuard]},
    {path: 'charity', component: CharityComponent },
    {path: 'charity/create', component: CharityCreateComponent, canActivate: [AuthGuard]},
    {path: 'charity/:charityId', component: CharityDetailComponent, canActivate: [AuthGuard]},
    // {path: 'notfound', }
    {path: '**', redirectTo: ''},
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
