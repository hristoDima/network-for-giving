import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {Charity} from '../../models/charity';
import {CharityService} from '../services/charity.service';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';

@Component({
    selector: 'app-charity-create',
    templateUrl: './charity-create.component.html',
    styleUrls: ['./charity-create.component.css']
})
export class CharityCreateComponent implements OnInit {
    charityForm = new FormGroup({
        name: new FormControl(''),
        description: new FormControl(''),
        budgetRequired: new FormControl(''),
        participantsRequired: new FormControl(''),
    });

    selectedFile: File;


    successModal = false;

    constructor(private charityService: CharityService,
                private router: Router) {
    }

    ngOnInit(): void {
    }

    onFileChanged(event) {
        this.selectedFile = event.target.files[0];
    }

    resetForm() {
        this.charityForm.reset();
    }

    addCharity() {
        const uploadImageData = new FormData();
        uploadImageData.append('imageFile', this.selectedFile, this.selectedFile.name);

        this.charityService.uploadThumbnail(uploadImageData).subscribe(response => {
            this.charityForm.addControl('thumbnail', new FormControl(this.selectedFile.name));
            console.log(this.charityForm.value);
            const charity: Charity = new Charity(this.charityForm.value);
            this.charityService.addCharity(charity).subscribe(_ => {
                this.successModal = true;
            });
        });
    }

}
