import {Component, OnInit} from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {User} from '../../models/user';
import {AccountService} from '../services/account.service';

@Component({
    selector: 'app-registration',
    templateUrl: './registration.component.html',
    styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
    public ages: number[] = [];

    profileForm = new FormGroup({
        fname: new FormControl(''),
        lname: new FormControl(''),
        gender: new FormControl(''),
        age: new FormControl(''),
        location: new FormControl(''),
        username: new FormControl(''),
        password: new FormControl(''),
    });


    constructor(private accountService: AccountService,
                private router: Router) {
    }

    ngOnInit(): void {
        for (let i = 15; i <= 130; i++) {
            this.ages.push(i);
        }
    }

    resetForm(): void {
        this.profileForm.reset();
    }

    register(): void {
        const newUser: User = new User(this.profileForm.value);
        this.accountService.registerUser(newUser).subscribe(_ => {
            this.router.navigate(['/login']);
        });
    }

}
