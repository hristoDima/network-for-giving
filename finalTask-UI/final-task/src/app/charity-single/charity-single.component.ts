import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {Charity} from '../../models/charity';
import {CharityService, CharityStats} from '../services/charity.service';
import {timeInterval} from 'rxjs/operators';

@Component({
    selector: 'app-charity-single',
    templateUrl: './charity-single.component.html',
    styleUrls: ['./charity-single.component.css']
})
export class CharitySingleComponent implements OnInit, OnChanges {

    @Input() charity: Charity;

    @Input() singleView = false;

    charityStats: CharityStats;

    // Image
    retrievedImage: any;
    base64Data: any;
    retrieveResponse: any;

    constructor(private charityService: CharityService) {
    }

    ngOnInit(): void {
        this.getThumbnail();
        this.getCharityStats();
    }

    ngOnChanges(): void {
        this.getThumbnail();
        this.getCharityStats();
    }

    getThumbnail(): void {
        this.charityService.getThumbnailImage(this.charity.thumbnail).subscribe(
            res => {
                this.retrieveResponse = res;
                this.base64Data = this.retrieveResponse.picByte;
                this.retrievedImage = 'data:image/jpeg;base64,' + this.base64Data;
            }
        );
    }

    getCharityStats(): void {
        this.charityService.getCharityStats(this.charity.id).subscribe(charityStats => {
            this.charityStats = charityStats;
            console.log(charityStats);
        });
    }
}
