import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CharitySingleComponent } from './charity-single.component';

describe('CharitySingleComponent', () => {
  let component: CharitySingleComponent;
  let fixture: ComponentFixture<CharitySingleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CharitySingleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CharitySingleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
