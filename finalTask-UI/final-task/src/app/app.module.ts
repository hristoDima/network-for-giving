import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';


import {AppComponent} from './app.component';
import {ClarityModule} from '@clr/angular';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CookieService} from 'ngx-cookie-service';

import {HeaderComponent} from './header/header.component';
import {AppRoutingModule} from './app-routing.module';
import {RegistrationComponent} from './registration/registration.component';
import {LoginComponent} from './login/login.component';
import {HomePageComponent} from './home-page/home-page.component';
import {CharityComponent} from './charity/charity.component';
import {CharityDetailComponent} from './charity-detail/charity-detail.component';
import {AccountDetailComponent} from './account-detail/account-detail.component';
import {CharityCreateComponent} from './charity-create/charity-create.component';
import {HttpConfigInterceptor} from './httpConfig.interceptor';
import { CharitySingleComponent } from './charity-single/charity-single.component';

@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        RegistrationComponent,
        LoginComponent,
        HomePageComponent,
        CharityComponent,
        CharityDetailComponent,
        AccountDetailComponent,
        CharityCreateComponent,
        CharitySingleComponent
    ],
    imports: [
        BrowserModule,
        ClarityModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        ReactiveFormsModule,
        HttpClientModule,

    ],
    providers: [
        {provide: HTTP_INTERCEPTORS, useClass: HttpConfigInterceptor, multi: true},
        CookieService,
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
