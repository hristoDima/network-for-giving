import {Component, OnInit} from '@angular/core';
import {Charity} from '../../models/charity';
import {CharityActivity, CharityService} from '../services/charity.service';
import {ActivatedRoute, Router, RouterModule} from '@angular/router';
import {UserActionService} from '../services/user-action.service';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
    selector: 'app-charity-detail',
    templateUrl: './charity-detail.component.html',
    styleUrls: ['./charity-detail.component.css']
})
export class CharityDetailComponent implements OnInit {

    public charity: Charity;

    public charityActivity: CharityActivity;

    participationModal: boolean;
    donationModal: boolean;

    donationForm = new FormGroup({
        donation: new FormControl('')
    });

    constructor(private charityService: CharityService,
                private route: ActivatedRoute,
                private userActionService: UserActionService) {
    }

    ngOnInit(): void {
        const id = +this.route.snapshot.paramMap.get('charityId');
        this.charityService.getCharity(id).subscribe(charity => this.charity = charity);
        this.charityService.getCharityActivity(id).subscribe(activity => this.charityActivity = activity);

        // modals
        this.participationModal = false;
        this.donationModal = false;
    }

    donate() {
        this.userActionService.donate(this.charity.id, this.donationForm.value).subscribe(_ => {
        this.donationModal = false;
        });
    }

    participate() {
        this.userActionService.participate(this.charity.id).subscribe(_ => {
        this.participationModal = false;
        });
    }
}
