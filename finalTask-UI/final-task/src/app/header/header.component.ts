import {Component, OnChanges, OnInit, SimpleChanges, DoCheck} from '@angular/core';
import {AuthenticationService} from '../services/authentication.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, DoCheck {
    public isUserAuthenticated: boolean;

    public logoutModal: boolean;

    constructor(private authenticationService: AuthenticationService,
                private router: Router) {
    }

    ngOnInit(): void {
        this.isUserAuthenticated = this.authenticationService.isAuthenticated();
        this.logoutModal = false;
    }

    ngDoCheck(): void {
        this.isUserAuthenticated = this.authenticationService.isAuthenticated();
    }

    logout() {
        this.authenticationService.logout().subscribe(_ => {
            this.isUserAuthenticated = this.authenticationService.isAuthenticated();
            this.logoutModal = false;
            this.router.navigate(['']);
        });
    }

}
