import {Component, OnInit} from '@angular/core';

import {Router} from '@angular/router';
import {AccountService} from '../services/account.service';
import {AuthenticationService} from '../services/authentication.service';

@Component({
    selector: 'app-home-page',
    templateUrl: './home-page.component.html',
    styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {

    constructor() {
    }

    ngOnInit(): void {
    }

}
