import {Injectable} from '@angular/core';
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor, HttpResponse, HttpErrorResponse
} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {map, catchError} from 'rxjs/operators';
import {AuthenticationService} from './services/authentication.service';

@Injectable()
export class HttpConfigInterceptor implements HttpInterceptor {

    constructor(private authenticationService: AuthenticationService) {
    }

    intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {


        // if (!request.headers.has('Content-Type')) {
        //     request = request.clone({headers: request.headers.set('Content-Type', 'application/json')});
        // }

        // request = request.clone({headers: request.headers.set('Accept', 'application/json')});

        request = request.clone({withCredentials: true});

        return next.handle(request).pipe(
            // map((event: HttpEvent<any>) => {
            //     if (event instanceof HttpResponse) {
            //         console.log('event--->>>', event);
            //     }
            //     return event;
            // }),
            catchError((error: HttpErrorResponse) => {
                if ([401, 403].indexOf(error.status) !== -1) {
                    // auto logout if 401 Unauthorized or 403 Forbidden response returned from api

                    this.authenticationService.logout().subscribe();
                    // location.reload(true);
                }
                // const err = error.error.message || error.statusText;
                // return throwError(err);


                // let data = {};
                // data = {
                //     reason: error && error.error && error.error.reason ? error.error.reason : '',
                //     status: error.status
                // };
                // console.log('someKindOfError');
                return throwError(error);
            }));
    }
}
